/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automaticreportgenerator.db;

/**
 *
 * @author Suresh Raju Pilli
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class SQLiteConnector {

    /**
     * Table Name User
     */
    private static final String TBL_NAME = "users";
    private String dbName = "memory";

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public SQLiteConnector() {
    }

    /**
     * Connect to a sample database
     *
     * @param dbName the Database name
     * @return Connection
     */
    public Connection connect(String dbName) {
        Connection conn = null;

        if (dbName == null) {
            dbName = this.dbName;
        }
        try {
            // db parameters
            String url = "jdbc:sqlite:" + dbName;
            // create a connection to the database
            conn = DriverManager.getConnection(url);
            //System.out.println("automaticreportgenerator.db.SQLiteConnector.connect()" + "Connection Created");
            // SQL statement for creating a new table
            String sql = "CREATE TABLE IF NOT EXISTS " + TBL_NAME + " (\n"
                    + "	id integer PRIMARY KEY,\n"
                    + "	name text NOT NULL\n"
                    + ");";
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            //System.out.println("automaticreportgenerator.db.SQLiteConnector.connect()" + TBL_NAME + " Created");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    /**
     * Insert data the specified connection
     *
     * @param conn the Connection
     * @param names the Names to insert
     * @return success or failure
     */
    public boolean insert(Connection conne, String[] names) {
        Connection conn = conne;
        String sql = "INSERT INTO " + TBL_NAME + " (name) VALUES(?)";
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            for (String name : names) {
                pstmt.setString(1, name);
                pstmt.executeUpdate();
            }
            return true;
        } catch (NullPointerException npe) {
            System.out.println("Connection is empty");
        } catch (SQLException e) {
            System.out.println("Connection exception");
            System.out.println(e.getMessage());
        }
        return false;
    }

    /**
     * select all rows in the users table
     *
     * @param conn the Connection
     * @return data in HashMap
     */
    public HashMap<Integer, String> getUserData(Connection conn) {
        HashMap<Integer, String> data = new HashMap<>();
        String sql = "SELECT id, name FROM " + TBL_NAME;

        try (Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("id") + ", " + rs.getString("name"));
                data.put(rs.getInt("id"), rs.getString("name"));
            }
        } catch (NullPointerException npe) {
            System.out.println("Connection is empty");
            return null;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return data;
    }
}
