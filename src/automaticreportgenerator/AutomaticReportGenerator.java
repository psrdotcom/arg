/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automaticreportgenerator;

import automaticreportgenerator.db.SQLiteConnector;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 *
 * @author Suresh Raju Pilli
 */
public class AutomaticReportGenerator {

    // max 1 thread
    static Semaphore semaphore = new Semaphore(1);

    static class MyLockerThread implements Runnable {
    
        String name = "";
        String[] DB_LIST;
        
        MyLockerThread(String name, String[] DB_LIST) {
            this.name = name;
            this.DB_LIST = DB_LIST;
        }
        
        @Override
        public void run() {
            Connection conn = null;
            boolean permit = false;
            try {
                permit = semaphore.tryAcquire(1, TimeUnit.SECONDS);
                if (permit) {
                    System.out.println(name + ": Semaphore acquired");
                    for(String dbName : DB_LIST) {
	                    SQLiteConnector sqliteconn = new SQLiteConnector();
	                    conn = sqliteconn.connect(dbName);
	                    HashMap<Integer, String> idNames = sqliteconn.getUserData(conn);
	                    for (Entry<Integer, String> userIdNames : idNames.entrySet()) {
	                        System.out.println(name + ": " + "userIdName: " + userIdNames.getKey() + ", " + userIdNames.getValue());
	                    }
                    }
                    // sleep 1 second
                    Thread.sleep(1000);
                } else {
                    //System.out.println(name + ": Could not acquire semaphore");
                    IntStream.range(0, 5).forEach(i -> System.out.println(name + ": " + i));
                }
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            } finally {
                // Closing connection
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException se) {

                    }
                }
                if (permit) {
                    semaphore.release();
                    System.out.println(name + ": Semaphore released");
                } else {
                    run();
                }

            }

        }
    
    }

    public static void main(String[] args) {

        final int THREAD_POOL_SIZE = 10;
        final String[] NAMES = new String[]{"Suresh", "Raju", "Pilli"};
        final String[] DB_LIST = new String[] {"testdb1.db","testdb2.db"};
        
        for(String dbName : DB_LIST) {
        	SQLiteConnector sqliteconn = new SQLiteConnector();
            Connection conn = sqliteconn.connect(dbName);
            sqliteconn.insert(conn, NAMES);
        }
        
        System.out.println("Total available Semaphore permits : "
                + semaphore.availablePermits());
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        IntStream.range(0, THREAD_POOL_SIZE).forEach(i -> executor.submit(new MyLockerThread("T"+i, DB_LIST)));
        executor.shutdown();
    }

}
